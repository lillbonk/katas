const {wantedLetter, longWord, fibonacci, fizzBuzz} = require('./app');

describe('The Awesome Tests', ()=> {

    test('Show the char that is used the most in string', ()=> {
        
        const words = ("Hello Beer lovers") 
        expect(wantedLetter(words)).toBe('e');

    });


    test('show teh longest substring', () =>{

        const word2 = ('Beeeeeer');
        expect(longWord(word2)).toBe(6);

    })

    test('Fibonacci.. .. Bless you!', () =>{

        const num = 8;
        expect(fibonacci(num)).toBe([0,1,1,2,3,5,8,13]);
    })


    test('Something with numbers and arrays :P ', () =>{

        const nums = 15;
        expect(fizzBuzz(nums)).toBe([ 1, 2, 'Fizz', 4, 'Buzz', 'Fizz', 7, 8, 'Fizz', 'Buzz', 11, 'Fizz', 13, 14, 'FizzBuzz' ]);


    })




})