const {filterMultiples,matcher} = require('./app');

describe('tester', () =>{

    const numbers = [2,4,6,7,9,10];
    const num = 3;
    test('multiple number filter', ()=>{

        expect(filterMultiples(numbers,num) ).toEqual([6,9]);
    })

    const arr = [1,'pizza',94,'taco','cheese']
    const arr2 = [2,'cheese','taco',9]
    test('Matching arrays', ()=> {

        expect(matcher(arr,arr2)).toEqual(['taco','cheese']);
    })

    const emails = ['dewals-els@mymail.com','dewald.els@hotmail.com'];
    test('Extract names form emails', ()=>{

       
        expect(emailToName(arr)).toEqual(['dewald els', 'dewald els']);
    })
    
});