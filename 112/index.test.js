const {describeNum,billSplit } = require('./index');

describe('My awesome tests!', () =>{

    test('Test num 4t', ()=>{

        let n = 4;
 
        expect(describeNum(n)).toEqual("The most brilliant exciting virtuous number is 4!");
        
    })
    test('Test num 13',() =>{
        let n=13;
        expect(describeNum(n)).toEqual("The most brilliant number is 13!");
    })
    test('Test num 21', ()=>{
        let n=21

        expect(describeNum(n)).toEqual("The most brilliant fantastic beautiful number is 21!")


    })


    test('Split the bills', () =>{

        food = ["S", "N", "S", "S"];
        cost = [13, 18, 15, 4];

        expect(billSplit(food, cost)).toEqual([41, 9])
            
    })

    test('Split the bills', () =>{

        food = ["N", "S", "N"];
        cost = [10, 10, 20];

        expect(billSplit(food, cost)).toEqual([25, 15])
            
    })

})