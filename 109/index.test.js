const {chunks,pad,reverseNum} = require('./index');

describe('A bunch of tests', () =>{

    
    test('Chunks....', () =>{
        let arr = ['a', 'b', 'c', 'd'];
        let div = 2;

        expect(chunks(arr,div)).toEqual(['a', 'b'], ['c', 'd'])

    });

    test('Pad...', () => {

       let str =  'Hello'
       let char = '!'
       let val = 8;

       expect(pad(str, char, val)).toEqual('Hello!!!')
    })



    test('reverse numbers', () =>{

        const nums= '1485979';
        expect(reverseNum(nums)).toBe('9795841');
    })
})