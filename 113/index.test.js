const {getBudget, vreplace, isogram} = require('./index');

describe('Kata 112 tests..', ()=>{

    test('All da money', ()=>{
       let budget= [
            { name: "John", age: 21, budget: 23000 },
            { name: "Steve", age: 32, budget: 40000 },
            { name: "Martin", age: 16, budget: 2700 }
           ]

        expect(getBudget(budget)).toBe(65700);
    })

    test('second money', ()=>{
        let budget = [
            { name: "John", age: 21, budget: 29000 },
            { name: "Steve", age: 32, budget: 32000 },
            { name: "Martin", age: 16, budget: 1600 }
           ]
        expect(getBudget(budget)).toBe(62600);
    })

    test('replace vowels with u', ()=>{

        let str= 'apples and bananas';
        let v = 'u';
        expect(vreplace(str, v)).toBe('upplus und bununus');
    })
   

        test('replace vowels with o', ()=>{
            let str='cheese casserole';
            let v = 'o';

        expect(vreplace(str, v)).toBe('chooso cossorolo');
    })



    test('replace vowels with e', ()=>{
        let str='stuffed jalapeno poppers';
        let v = 'e';

    expect(vreplace(str, v)).toBe('steffed jelepene peppers');
})

    test('isogram, whatever that is..', ()=>{

        let word = 'Algorism';

        expect(isogram(word)).toEqual(true)
    })

    test('isogram, whatever that is..', ()=>{

        let word = 'PasSword';

        expect(isogram(word)).toEqual(false)
    })

    test('isogram, whatever that is..', ()=>{

        let word = 'Consecutive';

        expect(isogram(word)).toEqual(false)
    })



    

})