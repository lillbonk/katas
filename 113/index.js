module.exports.getBudget = function (budget){

    return budget.reduce(function(tot, cur) {
        
        return tot + cur.budget;
       
        
      }, 0);
      
    

}

module.exports.vreplace = function(str, v){

     return str.replace(/[aoeiu]/gi, v)
}

module.exports.isogram = function(word){

    let result= word.toLowerCase().split('').sort().join('').match(/(.)\1+/g);

    if(result == null){
        return true;
    }else{
        return false;
    }

   
   
    
    
}
