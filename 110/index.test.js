const {jackpot,matchLast, anagram} = require('./index');

describe('Tests', ()=>{

    test('if 3 the same, JACKPOT', ()=>{

        const testJackpot = [7, 7, 8];
        expect(jackpot(testJackpot)).toBe(false)
    })

    test('Last matches the rest of array', () => {

        const arr = ["rsq", "6hi", "g", "rsq6hig"];
        
        expect(matchLast(arr)).toBe(true)
    })

    test('Anagram', () =>{

        word = ['Elvis', 'Lives'];
        expect(anagram(word)).toBe(true)
    })


})
