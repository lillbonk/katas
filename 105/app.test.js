const {sq, sq2,square, char, min} = require('./app');


describe('utils.Math', ()=> {

    const sqNum = 9
    test('square of given number', () =>{
        expect(sq(sqNum) ).toBe(3);
    });

    const n= 3
    test('square given number', () =>{
        expect(sq2(n) ).toBe(27);
    });

    const num = [2, 4, 8]
    test('square given number', () =>{
        expect(square(num) ).toEqual([4, 16,64]);
    });
    
    const chars = "abcbcc"
    test('give number of chars', () =>{
        expect(char(chars) ).toStrictEqual({'a':1,'b':2, 'c':3} );
    });
    
    const expected = ['c', 'Javascript'];
    const words = ['one', 'c', 'javascript', 'tacocat'];

    test('get the longest and shortest word')



});