module.exports.larger = function(num, num2) {
  let sum = 0;
  for (i = 0; i < num.length; i++) {
    sum += num[i];
  }
  if (sum > num2) {
    return true;
  } else {
    return false;
  }
};

module.exports.animalLegs = function(animals) {
  let sum = 0;
  for (i = 0; i < animals.length; i++) {
    sum = animals[0] * 2 + animals[1] * 4 + animals[2] * 4;
    return sum;
  }
};

module.exports.wordLength = function(words) {
  let array = words.map(x => x.length);
  return array;
};

module.exports.romanNumbersConv = function(modernNum) {
  const romanNumbers = { I: 1, V: 5, X: 10, L: 50, C: 100, D: 500, M: 1000 };

  for (i = 0; i < romanNumbers.length; i++) {
    if (modernNum > romanNumbers[i][0]) {
      console.log(romanNumbers);

      return romanNumbers[i][1];
    }
  }
};
