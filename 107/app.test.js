const {larger, animalLegs, wordLength, romanNumbersConv} = require('./app');

describe('Kata tests',()=> {


    test('SUM array, true if larger', () =>{

        const num = [10,20,30,41];
        const num2 = 100;
        expect(larger(num, num2)).toBe(true);
    })

    test('how many legs',() =>{

        const animals = [2,3,5];
        expect(animalLegs(animals)).toBe(36);
    })

    test('Lenght of words passed to an array', () =>{

        const words = ['I', 'love', 'beer'];
        expect(wordLength(words)).toEqual([1,4,4])
    })

    test(' romerska bågar!', () =>{

        
        const modernNum = 13;
        expect(romanNumbersConv(modernNum)).toBe('XIII')


    })


})