const colors = ['black', 'white', 'coral', 'steelblue','green','red', 'yellow','gray','lightblue','pink']

elbtn = document.getElementById('changeColor');



elbtn.addEventListener('click', function() {
   changeColor();
})




let colorIndex = 0;
function changeColor() {
    
    let elBody = document.getElementById('body');
    if( colorIndex >= colors.length ) {
        colorIndex = 0;
    }
    elBody.style.backgroundColor = colors[colorIndex];
    colorIndex++;
}

