elBtn2 = document.getElementById('changeColorBtn');

elBtn2.addEventListener('click', function(){
    manChangeColor();
})

;

function manChangeColor() {
    let elInput = document.getElementById('changeColorInput').value

    if (elInput.trim().length <7 || elInput.trim().indexOf('#') !==0){
        alert('Not a color');
    }
    
    let elBody = document.querySelector('body');
    elBody.style.backgroundColor = elInput;

}
